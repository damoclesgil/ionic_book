import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EbookPage } from './ebook';

@NgModule({
  imports: [
    IonicPageModule.forChild(EbookPage),
  ]
})
export class EbookPageModule {}
