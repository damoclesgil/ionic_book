import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EscolhaPage } from '../escolha/escolha';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  selecionaLivro(){
    console.log('Livro Selecionado');
    this.navCtrl.push(EscolhaPage);
  }

}
