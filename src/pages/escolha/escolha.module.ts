import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscolhaPage } from './escolha';

@NgModule({
  imports: [
    IonicPageModule.forChild(EscolhaPage),
  ],
})
export class EscolhaPageModule {}
