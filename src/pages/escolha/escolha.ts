import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { HomePage } from "../home/home";
import { EbookPage } from "../ebook/ebook";

@IonicPage()
@Component({
  selector: "page-escolha",
  templateUrl: "escolha.html"
})
export class EscolhaPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public plt: Platform
  ) {
    this.plt = plt;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad EscolhaPage");
  }
  homePageLink() {
    this.navCtrl.push(HomePage);
  }
  pageEbookson() {
    this.navCtrl.push(EbookPage);
  }
  exitApp() {
    this.plt.exitApp();
  }
}
