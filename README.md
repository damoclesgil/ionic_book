This is a starter template for [Ionic](http://ionicframework.com/docs/) projects.
Testing [Guide](https://ionicframework.com/docs/v1/guide/testing.html) projects.

## How to use this template

_This template does not work on its own_. The shared files for each starter are found in the [ionic2-app-base repo](https://github.com/ionic-team/ionic2-app-base).

To use this template, either create a new ionic project using the ionic node.js utility, or copy the files from this repository into the [Starter App Base](https://github.com/ionic-team/ionic2-app-base).

### With the Ionic CLI:

Take the name after `ionic2-starter-`, and that is the name of the template to be used when using the `ionic start` command below:

```bash
$ sudo npm install -g ionic cordova
$ ionic start myBlank blank
```

Then, to run it, cd into `myBlank` and run:

```bash
$ ionic cordova platform add android
$ ionic cordova run android
```

Helpers

```bash
$ ionic serve --lab
$ ionic g page nomedapagina
$ git push ionic master
```

Builds

```bash
$ ionic cordova build android --prod --release
$ ionic cordova build --release android // Esse aqui é o que mais uso
```
